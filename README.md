# BSD (ZFS) backup script

## This is just another backup shell script for ZFS on FreeBSD 

### version 25.03 (YY.MM)

Script is designed to creates snapshots of datasets and/or include also refular files/folders. It creates backup via zfs send, 
archive it by [TAR(1)](https://man.freebsd.org/cgi/man.cgi?query=tar&apropos=0&sektion=0&manpath=FreeBSD+14.0-RELEASE+and+Ports&arch=default&format=html) and compress with [lz4(1)](https://man.freebsd.org/cgi/man.cgi?query=lz4&sektion=&manpath=freebsd-release-ports) (lz4 is required to be installed on system)
lz4 is used, because of good compression ratio and speed.
Compressed archive is placed into backup location which can be dataset, folder or NFS mount.
Script is written to be executed by [CRON(8)](https://man.freebsd.org/cgi/man.cgi?query=cron&apropos=0&sektion=0&manpath=FreeBSD+14.0-RELEASE+and+Ports&arch=default&format=html) and 
with root privileges.

Expected period is daily, weekly, monthly or higher. 
In case, that you need multiple daily backups it is necessary to move out target archive from backup location. 

## Folder structure

`conf` include configuration files which datasets and/or files to backup (eg. `datasets.lst` and `regular.lst`)    
`log` contain log files     
`secret` contain secretfile with encryption password (if encryption in use)    

## Usage

1) Clone this repsitory: 

```
git clone https://gitlab.com/p.cerveny/bsd_backup_script
```

2) Move to desired location (`/root` will be used in this example) and setup rights and ownership:  
   (all commands in this example are expected run as root)

```
# mv bsd_backup_script/ /root
# chown -R root:wheel /root/bsd_backup_script
# chmod -R 750 /root/bsd_backup_script/bsd_bck_script.sh
```

3) Open `bsd_bck_script.sh` in text editor and modify `BACKUP_LOC` variable. It is location where backup archives are stored. It can be folder, dataset or NFS mount eg.:

`BACKUP_LOC=/zroot/backup`

4) Create `datasets.lst` config file in `conf/` folder.

`# touch /root/bsd_backup_script/conf/datasets.lst `


5) Open `conf/datasets.lst` in text editor and define list of datasets which need to be included in backup. 
Script takes datasets recursively.

Eg. if you have following structure:

```
zroot/data
zroot/data/mysql
zroot/data/mysql/db
zroot/data/mysql/logs
zroot/data/www
```

You can define each dataset one by one, or you can simply put zroot/data for all.
This approach can be use also for jails datasets or [bastille(8)](https://man.freebsd.org/cgi/man.cgi?query=bastille&apropos=0&sektion=0&manpath=FreeBSD+14.0-RELEASE+and+Ports&arch=default&format=html)


`datasets.lst`

```
zroot/data
zroot/bastille/jails/db
zroot/bastille/jails/www-apache
zroot/bastille/jails/monitoring
zroot/bastille/jails/nginx-proxy
```

6) Setup crontab.
Minimal period is daily. 


## Include regular files/folders into backup.

In case, that you want to include into backup also regular folders and files (traditional backup), create config file `conf/regular.lst` and define list of files/folders to be included.

`# touch /root/bsd_backup_script/conf/regular.lst `

Example:

```
/etc/pf.conf
/etc/rc.conf
/var/log
```

## Log and error log

When the script is run for the first time, it creates two log files: `bck_err_log` and `bck_script_log` in `log/` folder.   
 
`bck_err_log` contain all error messages.  
`bck_script_log` contain all information about backup progress and completion.

## Encryption
Target compressed backup archive can be also encrypted. To enable encryption create file in `secret/` folder with password.

Example:

```
# vi /root/bsd_backup_script/secret/enc_secret
```
Write your encryption password into this file and save.

Secure this file! Set strict permissions and make sure that is owned by `root:wheel`

```
# chown -R root:wheel /root/bsd_backup_script/secret
# chmod 700 /root/bsd_backup_script/secret
# chmod 600 /root/bsd_backup_script/secret/enc_secret
```
Name of file, which include encryption password can be define in `SECRETFILE` variable.

## Email notification  
Everytime when backup is executed, output of last backup report (including errors) is send to predefined users (`root` by default).    
To change recipient email modify `EMAIL` variable in `bsd_bck_script.sh`.   
For external emails, server requires proper SMTP server setup.  
For more recipients edit `/etc/aliases`.   
eg.: `backupadmins:   root, sysadmin, etc,` and run `newaliases`.

## Restore
Currently there is no automation script for restore (work in progress). Restore needs to be done manually.  
Output of `zfs list` and `datasets.lst` is by default included in each backup as reference for restore.
Path can be also determined from the backup file name.

Example: 

If encryption in use, decrypt first:
```
openssl enc -d -aes-256-cbc -pbkdf2 -in /zroot/backup/periodic_bck_DD_MM_YYYY.tar.lz4.enc -out /zroot/backup/periodic_bck_DD_MM_YYYY.tar.lz4
enter AES-256-CBC decryption password: <your_encryption_password>
```

```
# unlz4 /zroot/backup/periodic_bck_DD_MM_YYYY.tar.lz4
# tar xf /zroot/backup/periodic_bck_DD_MM_YYYY.tar
# zfs receive zroot/data/www < /zroot/backup/zroot/backup/periodic_bck_DD_MM_YYYY/zroot_data_www@periodic_bck_DD_MM_YYYY
```
