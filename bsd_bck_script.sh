#!/bin/sh

#Script stops immediately when whatever fails
set -e

#BSD 3-Clause License
#
#Copyright (c) 2024, Pavel Cerveny
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

##########################################################
#Define variables
##################################

#Target backup dataset path location
BACKUP_LOC=/zroot/backup

#Current timestamp
TIMESTAMP=periodic_bck_`date +%d_%m_%Y` 

#Log file
LOG=log/bck_script_log

#Error log file
ERRLOG=log/bck_err_log

#Encrypted secret file. 
SECRETFILE=secret/enc_secret

#Send email with logs when backup is executed
#Default: root
#For external emails, server requires proper mail server setup
EMAIL=root

#######################################
#Do not change anything behind this ! #
#######################################

#Absolute path
ABSOLUTE=$(CDPATH= cd -- "$(dirname -- "$0")" && pwd -P)

#################################
#Pre-checks
#################################

#Create timestamp in backup output log file
echo " " >> $ABSOLUTE/$LOG
echo "#################################" >> $ABSOLUTE/$LOG
echo "Backup executed:" >> $ABSOLUTE/$LOG
date >> $ABSOLUTE/$LOG
echo "---------------------------------" >> $ABSOLUTE/$LOG

#Create timestamp in error log file
echo "---------------------------------" >> $ABSOLUTE/$ERRLOG
echo "Errors for backup at:" >> $ABSOLUTE/$ERRLOG
date >> $ABSOLUTE/$ERRLOG

#Mail output of last backup run and errors to sysadmin
emailoutput() {
	(awk '/Backup executed/ { found = NR } END { if (found) { for (i = found; i <= NR; i++) print lines[i] } } { lines[NR] = $0 }' "$ABSOLUTE/$LOG"
	echo " "
	awk '/Errors for backup at/ { found = NR } END { if (found) { for (i = found; i <= NR; i++) print lines[i] } } { lines[NR] = $0 }' "$ABSOLUTE/$ERRLOG"
	) | mailx -s "Backup report" "$EMAIL" 2>>"$ABSOLUTE/$ERRLOG"
	exit
}

#Check permission, require run as root
if [ "$(id -u)" != 0 ]
	then 
		echo "Insufficient permission. Script needs to be run under root privileges or via sudo/doas." >> $ABSOLUTE/$ERRLOG
		emailoutput
		exit
fi

#Check if require file exists. List of datasets 
#file: datasets.lst 

if [ ! -f $ABSOLUTE/conf/datasets.lst ] 
	then 
		echo "Error. List of datasets do not exist. Please create file datasets.lst" >> $ABSOLUTE/$ERRLOG
		emailoutput
        	exit
fi

#Check if backup location exists
if [ ! -d $BACKUP_LOC ] 
	then 
		echo "Error. Backup target location $backup_loc does not exist. Please define correct location in backup_loc variable or create dataset." >> $ABSOLUTE/$ERRLOG
		emailoutput
		exit
fi

#Check if today backup already exists
if [ -f $BACKUP_LOC/$TIMESTAMP.tar.lz4 ]
	then 
		echo "Error. Today backup already exists. Please remove file $BACKUP_LOC/$TIMESTAMP.tar.lz4 and execute backup again." >> $ABSOLUTE/$ERRLOG
		emailoutput
		exit
fi

#Verify if lz4 is installed on system
if ! which lz4 > /dev/null
	then 
		echo "Error. lz4 compression pkg is not installed on system. Please install it. sudo pkg install liblz4" >> $ABSOLUTE/$ERRLOG
		emailoutput
		exit
fi 

#Verify if datasets in datasets.lst exists and create temporary file for further processing, this avoid backup exit in case that dataset does not exist
while read -r LINE;
do
        printf "Checking if dataset %s exists... " "$LINE" >>$ABSOLUTE/$LOG 2>>$ABSOLUTE/$ERRLOG
        if zfs list -H -o name | grep -x $LINE >/dev/null 2>>$ABSOLUTE/$ERRLOG
        then
                echo "OK" >> $ABSOLUTE/$LOG
                echo "$LINE" >> $ABSOLUTE/conf/datasets.lst.tmp 2>>$ABSOLUTE/$ERRLOG
        else
                echo "NOT FOUND skipping" >>$ABSOLUTE/$LOG 2>>$ABSOLUTE/$ERRLOG
        fi
done < $ABSOLUTE/conf/datasets.lst

#################################
#Exec backups
#################################

#Setup umask
umask 077

#Create timestamp folder in BACKUP_LOC
mkdir $BACKUP_LOC/$TIMESTAMP 2>>$ABSOLUTE/$ERRLOG

#Backup of regular files/folders
#Define regular files, which needs to be included into backup in conf/regular.lst

#zfs list and datasets.lst are always included for reference
zfs list >> $BACKUP_LOC/$TIMESTAMP/zfs_list 2>>$ABSOLUTE/$ERRLOG
echo "zfs list successfully created." >> $ABSOLUTE/$LOG
cp -p $ABSOLUTE/conf/datasets.lst $BACKUP_LOC/$TIMESTAMP 2>>$ABSOLUTE/$ERRLOG
echo "datasets.lst successfully copied." >> $ABSOLUTE/$LOG

#Check if config file for regular backup exists

if [ ! -f $ABSOLUTE/conf/regular.lst ]
then echo "Info. List of regular files to backup does not exist. Skipping..." >> $ABSOLUTE/$LOG
else
        while read -r LINE;
        do
                echo "Executing backup of $LINE" >> $ABSOLUTE/$LOG
		if [ -f $LINE ]
		then
			cp -p $LINE $BACKUP_LOC/$TIMESTAMP  2>>$ABSOLUTE/$ERRLOG
		else
			cp -Rp $LINE $BACKUP_LOC/$TIMESTAMP  2>>$ABSOLUTE/$ERRLOG
		fi
        done < $ABSOLUTE/conf/regular.lst
        echo "Regular files successfully copied." >> $ABSOLUTE/$LOG
fi

#Backup datasets.
#create hot snapshot, than create backup image of dataset and delete snapshot
#list of datasets is defined in datasets.lst eg. zroot/data, snapshots are created recursively, so define only the main dataset
#for bastille jails use eg. zroot/jails/myjail
while read -r LINE;
do 
	echo "Executing backup of $LINE" >> $ABSOLUTE/$LOG 
	TMP_FILENAME=$(echo $LINE@$TIMESTAMP | sed  's/\//_/g') 2>>$ABSOLUTE/$ERRLOG
	zfs snapshot -r $LINE@$TIMESTAMP && zfs send -R $LINE@$TIMESTAMP > $BACKUP_LOC/$TIMESTAMP/$TMP_FILENAME 2>>$ABSOLUTE/$ERRLOG
	#Cleanup
	zfs destroy -r $LINE@$TIMESTAMP 2>>$ABSOLUTE/$ERRLOG
	echo "Backup $LINE@$TIMESTAMP successfully created." >>$ABSOLUTE/$LOG
done < $ABSOLUTE/conf/datasets.lst.tmp

#Cleanup temporary file
rm -f $ABSOLUTE/conf/datasets.lst.tmp 2>>$ABSOLUTE/$ERRLOG

# Create an empty tarball
tar -cf $BACKUP_LOC/$TIMESTAMP.tar -T /dev/null

# Archive each file individually to avoid double space occupation
for file in "$BACKUP_LOC/$TIMESTAMP"/*; do
    tar -rf "$BACKUP_LOC/$TIMESTAMP.tar" "$file" 2>> $ABSOLUTE/$ERRLOG && rm -f "$file" 2>> $ABSOLUTE/$ERRLOG
done

#Remove remaining folder
rmdir $BACKUP_LOC/$TIMESTAMP 2>> $ABSOLUTE/$ERRLOG

# Compress the tar archive using lz4
lz4 -q --rm $BACKUP_LOC/$TIMESTAMP.tar 2>>$ABSOLUTE/$ERRLOG

#Encryption
#Check if encrypted secretfile exists.
if [ ! -f $ABSOLUTE/$SECRETFILE ]
then 
	echo "Encryption file does not exist. Skipping..." >> $ABSOLUTE/$LOG 
	echo "Compressed archive $BACKUP_LOC/$TIMESTAMP.tar.lz4 successfuly created." >> $ABSOLUTE/$LOG
	echo "--> Backup completed $(date)" >> $ABSOLUTE/$LOG
	emailoutput
	exit
else
	#If encryption file exits, encrypt backup file.
	KEYFILE=$(cat $ABSOLUTE/$SECRETFILE)
	if
		openssl enc -aes-256-cbc -salt -pbkdf2 -in $BACKUP_LOC/$TIMESTAMP.tar.lz4 -out $BACKUP_LOC/$TIMESTAMP.tar.lz4.enc -pass pass:$KEYFILE 2>>$ABSOLUTE/$ERRLOG
	then
		#Cleanup of unencrypted file
		rm -f $BACKUP_LOC/$TIMESTAMP.tar.lz4 2>>$ABSOLUTE/$ERRLOG
		echo "Encrypted archive $BACKUP_LOC/$TIMESTAMP.tar.lz4.enc successfuly created." >> $ABSOLUTE/$LOG
		echo "--> Backup completed $(date)" >> $ABSOLUTE/$LOG
		emailoutput
		exit
	else
		echo "Error! Encryption failed. File $BACKUP_LOC/$TIMESTAMP.tar.lz4.enc can be inconsistent. Please remove it if exists." >>$ABSOLUTE/$ERRLOG
		echo "Encryption failed. Please check error log for more info." >> $ABSOLUTE/$LOG
		echo "Compressed archive $BACKUP_LOC/$TIMESTAMP.tar.lz4 successfuly created." >> $ABSOLUTE/$LOG
		echo "--> Backup completed $(date)" >> $ABSOLUTE/$LOG
		emailoutput
		exit
	fi
fi
exit
